$(document).ready(function() {
    'use strict';
    $("#subs-datatable").DataTable({
        language : {
            paginate : {
                previous : "<i class='mdi mdi-chevron-left'>",
                next : "<i class='mdi mdi-chevron-right'>"
            },
            info : "Showing subs _START_ to _END_ of _TOTAL_",
            lengthMenu : 'Display <select class=\'custom-select custom-select-sm ml-1 mr-1\'><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="-1">All</option></select> subs'
        },
        pageLength : 10,
        columns : [ {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : false
        }],
        select : {
            style : "multi"
        },
        order : [[1, "desc"]],
        drawCallback : function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
        }
    });
    $(document.body).on('click', '.open-delete-anchor',function () {
        var subid = $(this).data('id');
        var subname = $(this).data('name');
        $(".modal-body .user-display-m").text( subname );
        $(".modal-body .user-id-m").val(subid);
    });
    $(document.body).on('click','.open-edit-sub',function () {
        console.log("Hello?");
        var subid = $(this).data('id');
        console.log(subid);
        var subname = $("span[data-name='"+subid+"']").eq(0).text();
        var subkinks = $("span[data-kinks='"+subid+"']").eq(0).text();
        var subactive = $(this).data('status');
        // noinspection EqualityComparisonWithCoercionJS
        if(subactive == 1) {
            $('#sub-edit-status option[value="active"]').prop('selected', true);
        }else {
            $('#sub-edit-status option[value="inactive"]').prop('selected', true);
        }
        $("#sub-edit-name").val( subname );
        $("#sub-edit-kinks").val( subkinks );
        $("#sub-edit-id").val(subid);
    });
    $("#screenshotModeSwitch").change(function () {
        if($(this).is(':checked')) {
            $('.sender-name').addClass('redact');
        }else {
            $('.sender-name').removeClass('redact');
        }
    });
});
