$(document).ready(function() {
    'use strict';
    $("#payments-datatable").DataTable({
        language : {
            paginate : {
                previous : "<i class='mdi mdi-chevron-left'>",
                next : "<i class='mdi mdi-chevron-right'>"
            },
            info : "Showing payments _START_ to _END_ of _TOTAL_",
            lengthMenu : 'Display <select class=\'custom-select custom-select-sm ml-1 mr-1\'><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="-1">All</option></select> payments'
        },
        pageLength : 20,
        columns : [{
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : true
        }, {
            orderable : false
        }],
        select : {
            style : "multi"
        },
        order : [[3, "desc"]],
        drawCallback : function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
        }
    });
    $(document.body).on('click','.open-delete-anchor',function () {
        var paymentid = $(this).data('id');
        $(".modal-body .payment-id-m").val(paymentid);
    });
    $("#screenshotModeSwitch").change(function () {
        if($(this).is(':checked')) {
            $('.sender-name').addClass('redact');
        }else {
            $('.sender-name').removeClass('redact');
        }
    });
});