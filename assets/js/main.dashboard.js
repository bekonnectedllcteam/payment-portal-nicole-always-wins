$("#screenshotRTModeSwitch").change(function () {
    if($(this).is(':checked')) {
        $('.sender-name-rt').addClass('redact');
    }else {
        $('.sender-name-rt').removeClass('redact');
    }
});

(Apex.grid = { padding: { right: 0, left: 0 } }), (Apex.dataLabels = { enabled: !1 });
var randomizeArray = function (e) {
    for (var r, t, a = e.slice(), o = a.length; 0 !== o; ) (t = Math.floor(Math.random() * o)), (r = a[(o -= 1)]), (a[o] = a[t]), (a[t] = r);
    return a;
};