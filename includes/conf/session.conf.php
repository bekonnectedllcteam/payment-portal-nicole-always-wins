<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}

/** Start sessions */
session_start();

/** Add initial $uid to anonymous user for non logged in users */
$uid = null;

/** @noinspection PhpUndefinedVariableInspection */
$accCollection = $naw->accounts;
/** Check for SESSION or COOKIE */
if(isset($_SESSION['uid'])){
    /** Looks like the user is logged in, correct $uid to their UID */
    $uid = $_SESSION['uid'];
}elseif (isset($_COOKIE['ssid'])) {
    $accCursor = $accCollection->findOne(['sessval' => base64_decode($_COOKIE['ssid'])]);
    if($accCursor != null) {
        $uid = $accCursor['_id'];
        $_SESSION['uid'] = $uid;
    }else {
        setcookie("ssid", "", time() - 3600);
    }
}
$user_info = [
    'display'   =>  'Error',
    'status'    =>  'Error',
    'image'     =>  'pet.jpg'
];
if($uid != null) {
    $accCursor = $accCollection->findOne(['_id' => new MongoDB\BSON\ObjectId($uid)]);
    if($accCursor != null) {
        $user_info = [
            'display'   =>  $accCursor['display'],
            'status'    =>  $accCursor['status'],
            'image'     =>  $accCursor['image']
        ];
    }
}
