<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
include_once "db.conf.php";
include_once "session.conf.php";

$privkey = $_SERVER['PRIVKEY'];
$hashkey = $_SERVER['HASHKEY'];