<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
/** @noinspection PhpUndefinedClassInspection */
$mongo = new MongoDB\Client($_SERVER['MONGODB']);
/** @noinspection PhpUndefinedFieldInspection */
$naw = $mongo->naw;