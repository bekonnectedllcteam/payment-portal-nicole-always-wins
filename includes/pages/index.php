<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
array_push($needed, 'vendor/apexcharts.min.js');
array_push($needed, "main.dashboard.js?v=20200821_2115E");
function compare_date($a, $b)
{
    return -($a['time'] <=> $b['time']);
}
date_default_timezone_set('Europe/Brussels');
$date = new DateTime('now');
$this_week = clone $date->modify('Sunday last week');
$last_week = $this_week->getTimestamp()-604800;
$two_week = $last_week-604800;
$three_week = $two_week-604800;
$four_week = $three_week-604800;
$five_week = $four_week-604800;
/** @noinspection PhpUndefinedVariableInspection */
$paymentsCollection = $naw->payments;
$transactionsCollection = $naw->transactions;
$subCollection = $naw->people;
$followerCollection = $naw->followers;
$followers = 0;
$followers_updated = 0;
$follow = $followerCollection->findOne(['prim' => true]);
if($follow != null) {
    $followers = $follow['followers'];
    $followers_updated = $follow['updated'];
}
$followers_trend = [];
$ftCursor = $followerCollection->find([], ['sort' => ['start' => 1]]);
foreach ($ftCursor as $follower) {
    if(!isset($follower['prim'])) {
        $t = $follower['start']+43200;
        array_push($followers_trend, [$t, $follower['min_followers'], $follower['max_followers']]);
    }
}
$subs = [];
$subCursor = $subCollection->find();
foreach ($subCursor as $sub) {
    $subs[(string) $sub['_id']] = $sub['name'];
}
$total = 0;
$payments = [];
$paymentCursor = $paymentsCollection->find(['time' => ['$gte' => $five_week]]);
foreach($paymentCursor as $payment) {
    $total += floatval(str_replace(',','',$payment['amount']));
    array_push($payments, [
        'person'    =>  $subs[(string) $payment['person']],
        'amount'    =>  str_replace(',','',$payment['amount']),
        'time'      =>  $payment['time'],
        'method'    =>  $payment['method'],
        'm'         =>  intval(date('m', $payment['time'])),
        'y'         =>  intval(date('Y', $payment['time'])),
        'type'      =>  1
    ]);
}
$transactionCursor = $transactionsCollection->find(['type' => 'send', 'created_at' => ['$gte' => $five_week]]);
foreach ($transactionCursor as $transaction) {
    $total += floatval($transaction['cash_amount']);
    array_push($payments, [
        'person'    =>  $subs['5f3fc52b5531db4c19622d28'],
        'amount'    =>  str_replace(',','',$transaction['cash_amount']),
        'time'      =>  $transaction['created_at'],
        'method'    => 'Bitcoin',
        'm'         =>  intval(date('m', $transaction['created_at'])),
        'y'         =>  intval(date('Y', $transaction['created_at'])),
        'type'      =>  0
    ]);
}
usort($payments, 'compare_date');
?>
<body class="loading" data-layout="detached" >
    <!-- Topbar Start -->
    <?php include "includes/pages/topbar.php"; ?>
    <!-- end Topbar -->

    <!-- Start Content-->
    <div class="container-fluid">
        <!-- Begin page -->
        <div class="wrapper">
            <!-- ========== Left Sidebar Start ========== -->
            <?php include "includes/pages/leftbar.php"; ?>
            <!-- ========== Left Sidebar End   ========== -->
            <div class="content-page">
                <div class="content">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item active">Nicole Always Wins</li>
                                    </ol>
                                </div>
                                <h4 class="page-title">Dashboard</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Tribute Summary -->
                        <div class="col-xl-3 col-lg-6">
                            <div class="card widget-flat">
                                <div class="card-body">
                                    <?php
                                    $the_amounts = [
                                            'this_week'     =>  0,
                                        'last_week'         =>  0,
                                        'two_week'          =>  0,
                                        'three_week'        =>  0,
                                        'four_week'         =>  0,
                                        'five_week'         =>  0,
                                        'this_month_total'  =>  0,
                                        'last_month_total'  =>  0,
                                        'bitcoin_total'     =>  0,
                                        'amazon_total'      =>  0,
                                        'paypal_total'      =>  0,
                                        'other_total'       =>  0,
                                        'bitcoin_count'     =>  0,
                                        'amazon_count'      =>  0,
                                        'paypal_count'      =>  0,
                                        'other_count'       =>  0,
                                        ];
                                    $current_month = intval(date('m'));
                                    $current_year = intval(date('Y'));
                                    $lastmonth = $current_month==1 ? 12 : $current_month - 1;
                                    $lastyear = $lastmonth==12 ? $current_year-1 : $current_year;

                                    foreach ($payments as $payment) {
                                        if($payment['m'] == $current_month) {
                                            $the_amounts['this_month_total'] += floatval($payment['amount']);
                                        }elseif($payment['m'] == $lastmonth && $payment['y'] == $lastyear) {
                                            $the_amounts['last_month_total'] += floatval($payment['amount']);
                                        }
                                        if($payment['time'] > $this_week->getTimestamp()) {
                                            $the_amounts['this_week'] += floatval($payment['amount']);
                                        }elseif ($payment['time'] > $last_week) {
                                            $the_amounts['last_week'] += floatval($payment['amount']);
                                        }elseif ($payment['time'] > $two_week) {
                                            $the_amounts['two_week'] += floatval($payment['amount']);
                                        }elseif ($payment['time'] > $three_week) {
                                            $the_amounts['three_week'] += floatval($payment['amount']);
                                        }elseif ($payment['time'] > $four_week) {
                                            $the_amounts['four_week'] += floatval($payment['amount']);
                                        }elseif ($payment['time'] > $five_week) {
                                            $the_amounts['five_week'] += floatval($payment['amount']);
                                        }
                                        switch (strtolower($payment['method'])) {
                                            case "bitcoin":
                                                $the_amounts['bitcoin_count']++;
                                                $the_amounts['bitcoin_total'] += floatval($payment['amount']);
                                                break;
                                            case "paypal":
                                                $the_amounts['paypal_count']++;
                                                $the_amounts['paypal_total'] += floatval($payment['amount']);
                                                break;
                                            case "amazon":
                                                $the_amounts['amazon_count']++;
                                                $the_amounts['amazon_total'] += floatval($payment['amount']);
                                                break;
                                            default:
                                                $the_amounts['other_count']++;
                                                $the_amounts['other_total'] += floatval($payment['amount']);
                                                break;
                                        }
                                    }
                                    ?>
                                    <h6 class="text-muted text-uppercase mt-0" title="Revenue">Tribute Summary</h6>
                                    <h3 class="mb-4 mt-2"><?php echo $total.' &euro;'; ?></h3>
                                    <div id="spark4" class="apex-charts mb-3" data-colors="#00c5dc"></div>

                                    <div class="row text-center">
                                        <div class="col-6">
                                            <h6 class="text-truncate d-block">Last Month</h6>
                                            <p class="font-18 mb-0"><?php echo $the_amounts['last_month_total'].' &euro;'; ?></p>
                                        </div>
                                        <div class="col-6">
                                            <h6 class="text-truncate d-block">Current Month</h6>
                                            <p class="font-18 mb-0"><?php echo $the_amounts['this_month_total'].' &euro;'; ?></p>
                                        </div>
                                    </div>
                                    <?php
                                    $add = <<<EOT
<!--suppress BadExpressionStatementJS, UnnecessaryLabelJS, CommaExpressionJS, JSUnnecessarySemicolon, JSMismatchedCollectionQueryUpdate -->
<script type="text/javascript">
$(document).ready(function () {
    "use strict";
    var n,
    a = ["#00c5dc"];
    (n = $("#spark4").data("colors")) && (a = n.split(","));
    var d = {
        chart: { type: "bar", height: 100, sparkline: { enabled: !0 } },
        plotOptions: { bar: { horizontal: !1, endingShape: "rounded", columnWidth: "55%" } },
        series: [{name:"Tributes", data: [{$the_amounts['five_week']}, {$the_amounts['four_week']}, {$the_amounts['three_week']}, {$the_amounts['two_week']}, {$the_amounts['last_week']}, {$the_amounts['this_week']}] }],
        labels: [' 5 weeks ago', '4 weeks ago', '3 weeks ago', '2 weeks ago', 'last week', 'this week'],
        stroke: { width: 3, curve: "smooth" },
        markers: { size: 0 },
        colors: a,
    };
        new ApexCharts(document.querySelector("#spark4"), d).render();
});
</script>
EOT;
                                    array_push($add_to_end, $add); //

                                    ?>
                                </div>
                            </div>
                        </div> <!-- end col-->
                        <!-- Tribute breakdown -->
                        <div class="col-xl-3 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Tribute Breakdown (6 Week)</h4>

                                    <div id="tribute-breakdown" class="apex-charts mb-4 mt-4"
                                         data-colors="#536de6,#ff5b5b,#10c469,#f9c851"></div>


                                    <div class="chart-widget-list">
                                        <p>
                                            <i class="mdi mdi-square text-primary"></i> Bitcoin
                                            <span class="float-right"><?php echo $the_amounts['bitcoin_total'].' &euro;'; ?></span>
                                        </p>
                                        <p>
                                            <i class="mdi mdi-square text-danger"></i> Amazon
                                            <span class="float-right"><?php echo $the_amounts['amazon_total'].' &euro;'; ?></span>
                                        </p>
                                        <p>
                                            <i class="mdi mdi-square text-success"></i> PayPal
                                            <span class="float-right"><?php echo $the_amounts['paypal_total'].' &euro;'; ?></span>
                                        </p>
                                        <p class="mb-0">
                                            <i class="mdi mdi-square text-warning"></i> Other
                                            <span class="float-right"><?php echo $the_amounts['other_total'].' &euro;'; ?></span>
                                        </p>
                                    </div>
                                </div> <!-- end card-body-->
                                <?php
                                $add = <<<EOT
<!--suppress BadExpressionStatementJS, UnnecessaryLabelJS, CommaExpressionJS, JSUnnecessarySemicolon, JSMismatchedCollectionQueryUpdate -->
<script type="text/javascript">
$(document).ready(function () {
    "use strict";
        var r,
        e = ["#727cf5", "#fa5c7c", "#0acf97", "#ffbc00"];
        (r = $("#tribute-breakdown").data("colors")) && (e = r.split(","));
        var t = {
            chart: { height: 213, type: "donut" },
            legend: { show: !1 },
            stroke: { colors: ["transparent"] },
            series: [{$the_amounts['bitcoin_count']}, {$the_amounts['amazon_count']}, {$the_amounts['paypal_count']}, {$the_amounts['other_count']}],
            labels: ["Bitcoin", "Amazon", "PayPal", "Other"],
            colors: e,
            responsive: [{ breakpoint: 480, options: { chart: { width: 200 }, legend: { position: "bottom" } } }],
        };
        new ApexCharts(document.querySelector("#tribute-breakdown"), t).render();
       });
</script>
EOT;
array_push($add_to_end, $add);
                                ?>
                            </div> <!-- end card-->
                        </div> <!-- end col-->
                        <!-- Recent Tributes-->
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title mb-3">Recent Tributes</h4>
                                    <!-- Custom Switch -->
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="screenshotRTModeSwitch">
                                        <label class="custom-control-label" for="screenshotRTModeSwitch">Screenshot mode</label>
                                        <br /><br />
                                    </div>
                                    <div data-simplebar style="max-height: 370px; overflow-x: hidden;">
                                        <?php
                                        $recents = array_slice($payments,0,10,true);
                                        foreach($recents as $recent) {
                                            ?>
                                            <div class="row py-1 align-items-center">
                                                <div class="col-auto">
                                                    <i class="mdi mdi-arrow-collapse-down text-success font-18"></i>
                                                </div>
                                                <div class="col pl-0">
                                                    <a class="text-body"><?php echo $recent['method']. " payment from <span class='sender-name-rt'>".$recent['person']."</span>" ?></a>
                                                    <p class="mb-0 text-muted"><small>
                                                            <?php
                                                            if(date('Ymd') == date('Ymd', $recent['time'])) {
                                                                echo "Today";
                                                            }elseif(date('Ymd', time()-(24*60*60)) == date('Ymd', $recent['time'])) {
                                                                echo "Yesterday";
                                                            }else {
                                                                echo date('Y-m-d', $recent['time']);
                                                            }
                                                            ?></small></p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="text-success font-weight-bold pr-2">+<?php echo $recent['amount']. " &euro;"; ?></span>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div> <!-- end slimscroll -->
                                </div>
                                <!-- end card-body -->
                            </div>
                            <!-- end card-->
                        </div>
                        <!-- <div class="col-xl-2 col-lg-5"></div> -->
                    </div>
                    <div class="row">
                        <div class="col-xl-10">
                            <div class="card widget-flat">
                                <div class="card-body">
                                    <div class="float-right">
                                        <i class="mdi mdi-twitter widget-icon bg-primary rounded-circle text-white"></i>
                                    </div>
                                    <h4 class="header-title mt-0" title="Followers">Followers</h4>
                                    <h3 class="mt-3 mb-3"><?php echo $followers; ?></h3>
                                    <div class="toolbar">
                                        <button id="one_month" class="btn btn-sm btn-light">1M</button>
                                        <button id="six_months" class="btn btn-sm btn-light">6M</button>
                                        <button id="one_year" class="btn btn-sm btn-light">1Y</button>
                                        <button id="ytd" class="btn btn-sm btn-light">YTD</button>
                                        <button id="all" class="btn btn-sm btn-light active">ALL</button>
                                    </div>
                                    <div id="area-chart-datetime" class="apex-charts" data-colors="#6c757d" style="min-height: 365px;">
                                        <div id="chart-timeline" class="apexcharts-canvas apexcharts8vzbjtki apexcharts-theme-light apexcharts-zoomable">
                                        </div>
                                    </div>
                                    <p class="mb-0">
                                        <span class="text-nowrap text-muted"><small><?php echo date('m/j/y @ g:i a',$followers_updated); ?></small></span>
                                    </p>
                                </div>
                            </div>
                            <?php
                            $s = "";
                            $a = "";
                            foreach ($followers_trend as $follower) {
                                $s .= "[".$follower[0].",".$follower[1]."],\n";
                                $a .= "[".$follower[0].",".$follower[2]."],\n";
                            }
                            $y = date("Y");
                            $ya = date("d M Y", strtotime('-1 year'));
                            $sa = date("d M Y", strtotime('-6 months'));
                            $oa = date("d M Y", strtotime('-1 months'));
                            $add = <<<EOT
<!--suppress UnnecessaryLabelJS, BadExpressionStatementJS, CommaExpressionJS, JSUnnecessarySemicolon, JSUnresolvedFunction -->
<script type="text/javascript">
var options = {
          series: [{
              name: "Least",
          data: [
            {$s}
          ]
        }, {
              name: "Most",
              data: [
                  {$a}
              ]
        }],
          chart: {
          id: 'follower-chart',
          type: 'line',
          height: 350,
          zoom: {
            autoScaleYaxis: true
          }
        },
        dataLabels: {
          enabled: false
        },
        markers: {
          size: 0,
          style: 'hollow',
        },
        colors: ['#FF4560','#008FFB'],
        xaxis: {
          type: 'datetime',
          min: new Date('22 Aug 2020').getTime(),
          tickAmount: 6,
        },
        tooltip: {
          x: {
            format: 'dd MMM yyyy'
          }
        },
        
        };

        var chart = new ApexCharts(document.querySelector("#chart-timeline"), options);
        chart.render();
      
      
        var resetCssClasses = function(activeEl) {
        var els = document.querySelectorAll('button')
        Array.prototype.forEach.call(els, function(el) {
          el.classList.remove('active')
        })
      
        activeEl.target.classList.add('active')
      }
      
      document
        .querySelector('#one_month')
        .addEventListener('click', function(e) {
          resetCssClasses(e)
      
          chart.zoomX(
            new Date('{$oa}').getTime(),
            new Date().getTime()
          )
        })
      
      document
        .querySelector('#six_months')
        .addEventListener('click', function(e) {
          resetCssClasses(e)
      
          chart.zoomX(
            new Date('{$sa}').getTime(),
            new Date().getTime()
          )
        })
      
      document
        .querySelector('#one_year')
        .addEventListener('click', function(e) {
          resetCssClasses(e)
          chart.zoomX(
            new Date('{$ya}').getTime(),
            new Date().getTime()
          )
        })
      
      document.querySelector('#ytd').addEventListener('click', function(e) {
        resetCssClasses(e)
      
        chart.zoomX(
          new Date('01 Jan {$y}').getTime(),
          new Date().getTime()
        )
      })
      
      document.querySelector('#all').addEventListener('click', function(e) {
        resetCssClasses(e)
      
        chart.zoomX(
          new Date('22 Aug 2020').getTime(),
          new Date().getTime()
        )
      });
</script>
EOT;
                            array_push($add_to_end, $add);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>