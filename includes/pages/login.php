<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
$error = "";
if(isset($_POST['login-btn'])) {
    $email = trim($_POST['emailaddress']);
    $pass = $_POST['password'];
    /** @noinspection PhpUndefinedVariableInspection */
    $accCollection = $naw->accounts;
    $account = null;
    $account = $accCollection->findOne(['email'   =>  $email]);
    if($account != null){
        if(password_verify($pass, $account['pass'])) {
            $exp = time() + (365 * 24 * 60 * 60); // 1 year of cookies!
            setcookie('ssid', base64_encode($account['sessval']), $exp);
            $uid = $account['_id'];
            $_SESSION['uid'] = $uid;
            /** @noinspection PhpUndefinedVariableInspection */
            if($page == "login") {
                header('Location: https://' . $_SERVER['SERVER_NAME']);
            }else {
                header('Location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
            }
            exit;
        }else {
            $error = "Invalid email/password combination.  Please try again!";
        }
    }else {
        $error = "Invalid email/password combination.  Please try again!";
    }
}
?>
<body class="loading authentication-bg">
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card">
                        <!-- Logo -->
                        <div class="card-header pt-4 pb-4 text-center bg-primary">
                            <a href="/">
                                <span></span>
                            </a>
                        </div>
                        <div class="card-body p-4">
                            <div class="text-center w-75 m-auto">
                                <?php if($error != ""){ ?>
                                <p class="text-muted mb-4"><?php echo $error; ?></p>
                                <?php } ?>
                                <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Sign In</h4>
                                <p class="text-muted mb-4">Enter your email address and password to access the portal.</p>
                            </div>
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="emailaddress">Email address</label>
                                    <input class="form-control" type="email" id="emailaddress" name="emailaddress" required="" placeholder="Enter your email">
                                </div>
                                <div class="form-group">
                                    <a href="/forgot" class="text-muted float-right"><small>Forgot your password?</small></a>
                                    <label for="password">Password</label>
                                    <div class="input-group input-group-merge">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter your password">
                                        <div class="input-group-append" data-password="false">
                                            <div class="input-group-text">
                                                <span class="password-eye"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary" type="submit" id="login-btn" name="login-btn"> Log In </button>
                                </div>
                            </form>
                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->
                    <!-- end row -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->