<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
array_push($add_to_end, '<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js" type="text/javascript" crossorigin="anonymous"></script>');
array_push($add_to_end, '<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/hmac-sha256.min.js" type="text/javascript" crossorigin="anonymous"></script>');
array_push($add_to_end, '<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/enc-base64.min.js" type="text/javascript" crossorigin="anonymous"></script>');
array_push($add_to_end, "<script src='/sio/socket.io/socket.io.js' type='text/javascript' crossorigin='anonymous'></script>");
// CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256("verificationshit",secret))
array_push($add_to_end, '<script src="/assets/js/coinbase.js/?v='.time().'" type="text/javascript"></script>');
/** @noinspection PhpUndefinedVariableInspection */
$subCollection = $naw->people;
$pet = $subCollection->findOne(['type' => 1]);
$petName = "Nicole's Pet";
if($pet != null) {
    $petName = $pet['name'];
}
?>
<body class="loading" data-layout="detached" >
<!-- Topbar Start -->
<?php include "includes/pages/topbar.php"; ?>
<!-- end Topbar -->

<!-- Start Content-->
<div class="container-fluid">
    <!-- Begin page -->
    <div class="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <?php include "includes/pages/leftbar.php"; ?>
        <!-- ========== Left Sidebar End   ========== -->
        <div class="content-page">
            <div class="content">
                <?php // BTC price times BTC balance equals rough USD balance | round up to nearest .00 after 5 ?>
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="/">Nicole Always Wins</a></li>
                                    <li class="breadcrumb-item active">Coinbase</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Coinbase</h4>
                            <small class="text-muted">Status: <span id="server-status"></span></small>
                            <br /><br />
                        </div>
                    </div>
                </div>
                <!-- End Breadcrumb -->
                <!-- Start Server messages -->
                <div class="row">
                    <div class="col-xl-3 col-lg-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-currency-eur widget-icon bg-primary rounded-circle text-white"></i>
                                </div>
                                <h4 class="header-title mt-0" title="Balance"><?php echo $petName; ?>'s Balance</h4>
                                <h3 class="mt-3 mb-3" id="euro-balance"></h3>
                                <a style="text-decoration: none;" id="refresh-balance"><span class="text-nowrap text-muted"><small>Refresh</small></span></a>
                            </div>
                        </div>
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="form-group text-center">
                                    <label for="amount-to-send" >Amount to Send</label>
                                    <input type="text" class="form-control" data-toggle="input-mask" data-mask-format="#0.00" data-reverse="true" id="amount-to-send" placeholder="1.00">
                                    <br />
                                    <button type="button" class="btn btn-success" id="send-button"><i class="mdi mdi-send mr-1"></i> <span>Send</span> </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
