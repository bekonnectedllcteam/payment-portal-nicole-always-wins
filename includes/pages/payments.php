<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
array_push($needed, "vendor/jquery.dataTables.min.js");
array_push($needed, "vendor/dataTables.bootstrap4.js");
array_push($needed, "vendor/dataTables.responsive.min.js");
array_push($needed, "vendor/responsive.bootstrap4.min.js");
array_push($needed, "vendor/dataTables.checkboxes.min.js");
array_push($needed, "pages/payments.min.js?v=20200823_1638E");
date_default_timezone_set('Europe/Brussels');
/** @noinspection PhpUndefinedVariableInspection */
$paymentsCollection = $naw->payments;
$transactionsCollection = $naw->transactions;
$subCollection = $naw->people;
$success="";
$error="";
if(isset($_POST['addpayment']) && isset($_POST['subid']) && isset($_POST['paymentmethod']) && isset($_POST['paymentamount'])) {
    // Process adding payment
    $insertResult = $paymentsCollection->insertOne([
            'person'    =>  new MongoDB\BSON\ObjectId($_POST['subid']),
            'amount'    =>  $_POST['paymentamount'],
            'method'    =>  $_POST['paymentmethod'],
            'time'  =>      time()
    ]);
    if($insertResult->getInsertedCount() == 1) {
        $success = "Payment was successfully added!";
    }
}elseif(isset($_POST['removepayment']) && isset($_POST['paymentid'])) {
    $deleteRecord = $paymentsCollection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($_POST['paymentid'])]);
    if($deleteRecord != null) {
        $success = "Payment successfully deleted!";
    }else {
        $error = "There was an error removing the payment from the database!";
    }
}
$subs = [];
$subCursor = $subCollection->find();
foreach ($subCursor as $sub) {
    $subs[(string) $sub['_id']] = $sub['name'];
}
$payments = [];
$paymentCursor = $paymentsCollection->find();
foreach($paymentCursor as $payment) {
    $leDate = date("Y-m-d H:i", $payment['time']);
    array_push($payments, [(string) $payment['_id'] => [
        'person'    =>  $subs[(string) $payment['person']],
        'amount'    =>  $payment['amount'],
        'time'      =>  $leDate,
        'method'    =>  $payment['method'],
        'type'      =>  1
    ]]);
}
$transactionCursor = $transactionsCollection->find(['type' => 'send']);
foreach ($transactionCursor as $transaction) {
    $leDate = date("Y-m-d H:i", $transaction['created_at']);
    array_push($payments, [(string) $transaction['_id'] => [
        'person'    =>  $subs['5f3fc52b5531db4c19622d28'],
        'amount'    =>  $transaction['cash_amount'],
        'time'      =>  $leDate,
        'method'    => 'Bitcoin',
        'type'      =>  0
    ]]);
}
?>
<body class="loading" data-layout="detached" >
<!-- Topbar Start -->
<?php include "includes/pages/topbar.php"; ?>
<!-- end Topbar -->

<!-- Start Content-->
<div class="container-fluid">
    <!-- Begin page -->
    <div class="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <?php include "includes/pages/leftbar.php"; ?>
        <!-- ========== Left Sidebar End   ========== -->
        <div class="content-page">
            <div class="content">
                <div id="add-payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body">
                                <div class="text-center mt-2 mb-4">
                                    <span></span>
                                </div>

                                <form class="pl-3 pr-3" action="" method="post">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="subid">Sub</label>
                                            </div>
                                            <select class="custom-select" id="subid" name="subid">
                                                <option selected>Choose...</option>
                                                <?php
                                                foreach($subs as $id => $sub) {

                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $sub; ?></option>
                                                        <?php

                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="paymentmethod">Method</label>
                                            </div>
                                            <select class="custom-select" id="paymentmethod" name="paymentmethod">
                                                <option selected value="Amazon">Amazon</option>
                                                <option value="PayPal">PayPal</option>
                                                <option value="Bitcoin">Bitcoin</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="paymentamount">Amount</label>
                                        <input class="form-control" id="paymentamount" type="text" name="paymentamount">
                                    </div>

                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="submit" name="addpayment" id="addpayment">Add Payment</button>
                                    </div>

                                </form>

                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- Delete Alert Modal -->
                <div id="delete-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <form class="pl-3 pr-3" action="" method="post">
                            <div class="modal-content modal-filled bg-danger">
                                <div class="modal-body p-4">
                                    <div class="text-center">
                                        <i class="dripicons-wrong h1"></i>
                                        <h4 class="mt-2">Are you sure?</h4>
                                        <p class="mt-3">Are you sure you wish to delete this transaction?</p>
                                        <input type="hidden" value="" class="payment-id-m" name="paymentid" id="paymentid" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-dark my-2" id="removepayment" name="removepayment">Continue</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </form>
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="/">Nicole Always Wins</a></li>
                                    <li class="breadcrumb-item active">Payments</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Payments</h4>
                        </div>
                    </div>
                </div>
                <?php
                if(strlen($error) > 1) {
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="dripicons-wrong mr-2"></i><strong>Error - </strong> <?php echo $error; ?>
                    </div>
                    <?php
                }
                if(strlen($success) > 1) {
                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="dripicons-checkmark mr-2"></i><strong>Success - </strong> <?php echo $success; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Custom Switch -->
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="screenshotModeSwitch">
                                    <label class="custom-control-label" for="screenshotModeSwitch">Screenshot mode</label>
                                    <br /><br />
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-4">
                                        <button  class="btn btn-danger mb-2" data-toggle="modal" data-target="#add-payment-modal"><i class="mdi mdi-plus-circle mr-2"></i> Add Payment</button>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-centered w-100 nowrap dt-responsive" id="payments-datatable"> <!-- dt-responsive for the plus/minus instead of side to side -->
                                        <thead class="thead-light">
                                        <tr>
                                            <th class="all">Name</th>
                                            <th>Amount</th>
                                            <th>Method</th>
                                            <th>Date</th>
                                            <th style="width: 85px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        foreach ($payments as $l) {
                                            foreach($l as $id => $payment) {
                                                ?>

                                                <tr>
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                            <span class="sender-name"><?php echo $payment['person']; ?></span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        echo  $payment['amount']. "	&euro;";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $payment['method']; ?>
                                                    </td>
                                                    <td>

                                                        <?php echo $payment['time']; ?>
                                                    </td>

                                                    <td class="table-action">
                                                        <?php
                                                        if($payment['type'] == 1) {
                                                          ?>
                                                            <a data-toggle="modal" data-target="#delete-alert-modal" class="open-delete-anchor action-icon" data-id="<?php echo $id; ?>"> <i class="mdi mdi-delete"></i></a>
                                                                <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
</div>