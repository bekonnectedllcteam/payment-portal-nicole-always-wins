<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
// TO-DO: Actually make this page work | more or less irrelevant though
if(isset($pages[1]) && strlen($pages[1]) == 40) {
    /** @noinspection PhpUndefinedVariableInspection */
    $forgottenCollection = $naw->forgotten;
    $forgottenOne = $forgottenCollection->findOne(['key' => $pages[1]]);
    if($forgottenOne != null) {
        if(isset($_POST['updatebtn']) && isset($_POST['updatepass'])) {
            /** @noinspection PhpUndefinedVariableInspection */
            $accCollection->updateOne(['_id' => $forgottenOne['account']], ['$set' => ['pass' => password_hash($_POST['updatepass'], PASSWORD_BCRYPT)]]);
            $forgottenCollection->deleteOne(['key' => $pages[1]]);
            header('Location: https://' . $_SERVER['SERVER_NAME']);
        }else {
            ?>
            <div class="account-pages mt-5 mb-5">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="card">
                                <!-- Logo -->
                                <div class="card-header pt-4 pb-4 text-center bg-primary">
                                    <a href="/">
                                        <span></span>
                                    </a>
                                </div>

                                <div class="card-body p-4">

                                    <div class="text-center w-75 m-auto">
                                        <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Reset Password</h4>
                                        <p class="text-muted mb-4">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                                    </div>

                                    <form action="" method="post">
                                        <div class="form-group mb-3">
                                            <label for="updatepass">Password</label>
                                            <div class="input-group input-group-merge">
                                                <input type="password" id="updatepass" name="updatepass" class="form-control" placeholder="Enter new password">
                                                <div class="input-group-append" data-password="false">
                                                    <div class="input-group-text">
                                                        <span class="password-eye"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0 text-center">
                                            <button class="btn btn-primary" id="updatebtn" name="updatebtn" type="submit">Reset Password</button>
                                        </div>
                                    </form>
                                </div> <!-- end card-body-->
                            </div>
                            <!-- end card -->

                            <div class="row mt-3">
                                <div class="col-12 text-center">
                                    <p class="text-muted">Back to <a href="/login" class="text-muted ml-1"><b>Log In</b></a></p>
                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->

                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
            <!-- end page -->
            <?php
        }
    }else {
        include "errors/404.php";
    }
}else {
?>
<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card">
                    <!-- Logo -->
                    <div class="card-header pt-4 pb-4 text-center bg-primary">
                        <a href="/">
                            <span></span>
                        </a>
                    </div>

                    <div class="card-body p-4">

                        <div class="text-center w-75 m-auto">
                            <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Reset Password</h4>
                            <p class="text-muted mb-4">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                        </div>

                        <form action="#">
                            <div class="form-group mb-3">
                                <label for="emailaddress">Email address</label>
                                <input class="form-control" type="email" id="emailaddress" required="" placeholder="Enter your email">
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary" type="submit">Reset Password</button>
                            </div>
                        </form>
                    </div> <!-- end card-body-->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p class="text-muted">Back to <a href="/login" class="text-muted ml-1"><b>Log In</b></a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->
<?php
}
