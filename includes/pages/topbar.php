<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
?>
<div class="navbar-custom topnav-navbar topnav-navbar-dark">
    <div class="container-fluid">

        <!-- LOGO -->
        <a href="/" class="topnav-logo">
                    <span class="topnav-logo-lg">

                    </span>
            <span class="topnav-logo-sm">

                    </span>
        </a>

        <ul class="list-unstyled topbar-right-menu float-right mb-0">

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" id="topbar-userdrop" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">
                            <span class="account-user-avatar">
                                <img src="/assets/images/users/<?php /** @noinspection PhpUndefinedVariableInspection */
                                echo $user_info['image']; ?>" alt="user-image" class="rounded-circle ">
                            </span>
                    <span>
                                <span class="account-user-name"><?php echo $user_info['display']; ?></span>
                                <span class="account-position"><?php echo $user_info['status']; ?></span>
                            </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown" aria-labelledby="topbar-userdrop">
                    <!-- item-->
                    <!--<div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome !</h6>
                    </div>-->

                    <!-- item-->
                    <a href="https://twitter.com/nicolespet" target="_blank" class="dropdown-item notify-item">
                        <i class="mdi mdi-lifebuoy mr-1"></i>
                        <span>Support</span>
                    </a>

                    <!-- item-->
                    <a href="/logout" class="dropdown-item notify-item">
                        <i class="mdi mdi-logout mr-1"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </li>

        </ul>
        <a class="button-menu-mobile disable-btn">
            <div class="lines">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </a>
    </div>
</div>
