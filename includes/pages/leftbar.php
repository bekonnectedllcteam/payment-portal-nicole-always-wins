<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
?>
<div class="left-side-menu left-side-menu-detached">
    <div class="leftbar-user">
            <img src="/assets/images/users/<?php /** @noinspection PhpUndefinedVariableInspection */
            echo $user_info['image']; ?>" alt="user-image" height="42" class="rounded-circle shadow-sm">
            <span class="leftbar-user-name"><?php echo $user_info['display']; ?></span>

    </div>

    <!--- Sidemenu -->
    <ul class="metismenu side-nav">

        <li class="side-nav-title side-nav-item">Navigation</li>

        <li class="side-nav-item<?php /** @noinspection PhpUndefinedVariableInspection */
        if($page == ""){ echo " mm-active"; } ?>">
            <a href="/" class="side-nav-link">
                <i class="uil-home-alt"></i>
                <span> Dashboard </span>
            </a>
        </li>

        <li class="side-nav-item<?php if($page == "subs"){ echo " mm-active"; } ?>">
            <a href="/subs" class="side-nav-link">
                <i class="uil-user-circle"></i>
                <span> Submissives </span>
            </a>
        </li>

        <li class="side-nav-item<?php if($page == "payments"){ echo " mm-active"; } ?>">
            <a href="/payments" class="side-nav-link">
                <i class="uil-money-bill"></i>
                <span> Payments </span>
            </a>
        </li>
        <li class="side-nav-item<?php if($page == "coinbase"){ echo " mm-active"; } ?>">
            <a href="/coinbase" class="side-nav-link">
                <i class="uil-bitcoin-circle"></i>
                <span> Coinbase </span>
            </a>
        </li>
    </ul>
    <!-- End Sidebar -->

    <div class="clearfix"></div>
    <!-- Sidebar -left -->
</div>