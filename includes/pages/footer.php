<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
?>
        <script src="/assets/js/vendor.min.js"></script>
        <script src="/assets/js/app.min.js?v=202008201528E"></script>
<?php
/** @noinspection PhpUndefinedVariableInspection */
foreach($needed as $need) {
    if(strpos($need, ".js") !== false){
        echo '<script src="/assets/js/'.$need.'"></script>';
    }
}
/** @noinspection PhpUndefinedVariableInspection */
foreach ($add_to_end as $add) {
    echo $add;
}
?>
        <script src="/assets/js/main.js?v=20200820_1447E"></script>
    </body>