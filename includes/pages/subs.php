<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
array_push($needed, "vendor/jquery.dataTables.min.js");
array_push($needed, "vendor/dataTables.bootstrap4.js");
array_push($needed, "vendor/dataTables.responsive.min.js");
array_push($needed, "vendor/responsive.bootstrap4.min.js");
array_push($needed, "vendor/dataTables.checkboxes.min.js");
array_push($needed, "pages/submissives.min.js?v=20200823_1638E");
$error = "";
$success = "";
/** @noinspection PhpUndefinedVariableInspection */
$subCollection = $naw->people;
$paymentsCollection = $naw->payments;
$transactionsCollection = $naw->transactions;
if(isset($_POST['addsub']) && isset($_POST['subsince']) && isset($_POST['subname'])) {
    if($subCollection->findOne(['name' => trim($_POST['subname'])], ['collation' => ['locale' => 'en', 'strength' => 2]]) == null) {
        $insertResult = $subCollection->insertOne([
            'name'      =>  trim($_POST['subname']),
            'added'     =>  trim($_POST['subsince']),
            'type'      =>  0,
            'active'    => true
        ]);
        if($insertResult->getInsertedCount() == 1) {
            $success = $_POST['subname']. " successfully added!";
        }
    }else {
        $error = "Sub name already in the database!";
    }
}elseif(isset($_POST['removesub']) && isset($_POST['subid'])) {
    $deleteRecord = $subCollection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($_POST['subid'])]);
    if($deleteRecord != null) {
        if(isset($_POST['deletetributes'])) {
            // Also delete payment history
            $deleteRecord2 = $paymentsCollection->deleteMany(['person' => new MongoDB\BSON\ObjectId($_POST['subid'])]);
            if($deleteRecord2 != null) {
                $success = "Sub and their tribute history was successfully removed!";
            }else {
                $success = "Sub was successfully removed!";
                $error = "There was an error deleting the subs tribute history (or they had none to begin with)!";
            }
        }else {
            $success = "Sub was successfully removed!";
        }
    }else {
        $error = "There was an error deleting the sub from the database!";
    }
}elseif(isset($_POST['sub-edit-go']) && isset($_POST['sub-edit-id']) && isset($_POST['sub-edit-status']) && isset($_POST['sub-edit-name']) && isset($_POST['sub-edit-kinks'])) {
    $active = false;
    if($_POST['sub-edit-status'] == "active") {
        $active = true;
    }
    $updateRecord = $subCollection->updateOne(['_id' => new MongoDB\BSON\ObjectId($_POST['sub-edit-id'])], ['$set' => ['name' => $_POST['sub-edit-name'], 'active' => $active, 'kinks' => $_POST['sub-edit-kinks']]]);
    if($updateRecord != null) {
        $success = "Sub was successfully updated!";
    }else {
        $error = "There was an error updating the sub in the database!";
    }
}
$subs = [];
$subCursor = $subCollection->find();
foreach ($subCursor as $sub) {
    array_push($subs, [(string) $sub['_id'] => [
        'name'      =>  $sub['name'],
        'added'     =>  $sub['added'],
        'type'      =>  $sub['type'],
        'kinks'     =>  isset($sub['kinks']) ? $sub['kinks'] : "",
        'active'    =>  $sub['active']
    ]]);
}
?>
<body class="loading" data-layout="detached" >
<!-- Topbar Start -->
<?php include "includes/pages/topbar.php"; ?>
<!-- end Topbar -->

<!-- Start Content-->
<div class="container-fluid">
    <!-- Begin page -->
    <div class="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <?php include "includes/pages/leftbar.php"; ?>
        <!-- ========== Left Sidebar End   ========== -->
        <div class="content-page">
            <div class="content">
                <!-- ADD SUB MODAL -->
                <div id="add-sub-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="text-center mt-2 mb-4">
                                        <span></span>
                                </div>

                                <form class="pl-3 pr-3" action="" method="post">
                                    <div class="form-group">
                                        <label for="subname">Name</label>
                                        <input class="form-control" type="text" id="subname" name="subname" required="" placeholder="Submissives Name">
                                    </div>

                                    <div class="form-group">
                                        <label for="subsince">Sub Since</label>
                                        <input class="form-control" type="date" id="subsince" name="subsince" required="" placeholder="">
                                    </div>

                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="submit" name="addsub" id="addsub">Add Sub</button>
                                    </div>

                                </form>

                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- Delete Alert Modal -->
                <div id="delete-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <form class="pl-3 pr-3" action="" method="post">
                            <div class="modal-content modal-filled bg-danger">
                                <div class="modal-body p-4">
                                    <div class="text-center">
                                        <i class="dripicons-wrong h1"></i>
                                        <h4 class="mt-2">Are you sure?</h4>
                                        <p class="mt-3">Are you sure you wish to delete <span class="user-display-m">&nbsp;</span>?</p>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="deletetributes" name="deletetributes">
                                            <label class="custom-control-label" for="deletetributes">Delete tribute history</label>
                                        </div>
                                        <input type="hidden" value="" class="user-id-m" name="subid" id="subid" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-dark my-2" id="removesub" name="removesub">Continue</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </form>
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- EDIT SUB MODAL -->
                <div id="edit-sub-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="text-center mt-2 mb-4">
                                </div>

                                <form class="pl-3 pr-3" action="" method="post">

                                    <div class="form-group">
                                        <label for="sub-edit-name">Edit Sub</label>
                                        <input class="form-control user-edit-display-m" type="text" id="sub-edit-name" name="sub-edit-name" required="" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="sub-edit-kinks" class="sr-only">Kinks</label>
                                        <input class="form-control user-edit-kinks-m" type="text" id="sub-edit-kinks" name="sub-edit-kinks" placeholder="Kinks">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="sub-edit-status">Status</label>
                                            </div>
                                            <select class="custom-select" id="sub-edit-status" name="sub-edit-status">
                                                <option value="active">Active</option>
                                                <option value="inactive" selected>Inactive</option>
                                            </select>
                                        </div>
                                    </div>


                                    <input type="hidden" required="" id="sub-edit-id" value="" name="sub-edit-id" class="user-edit-id-m">


                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="submit" name="sub-edit-go" id="sub-edit-go">Edit Sub</button>
                                    </div>

                                </form>

                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="/">Nicole Always Wins</a></li>
                                    <li class="breadcrumb-item active">Subs</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Subs</h4>
                        </div>
                    </div>
                </div>
                <?php
                if(strlen($error) > 1) {
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="dripicons-wrong mr-2"></i><strong>Error - </strong> <?php echo $error; ?>
                    </div>
                <?php
                }
                if(strlen($success) > 1) {
                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="dripicons-checkmark mr-2"></i><strong>Success - </strong> <?php echo $success; ?>
                    </div>
                <?php
                }
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Custom Switch -->
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="screenshotModeSwitch">
                                    <label class="custom-control-label" for="screenshotModeSwitch">Screenshot mode</label>
                                    <br /><br />
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-4">
                                        <button  class="btn btn-danger mb-2" data-toggle="modal" data-target="#add-sub-modal"><i class="mdi mdi-plus-circle mr-2"></i> Add Sub</button>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-centered w-100 text-wrap dt-responsive" id="subs-datatable"> <!-- dt-responsive for the plus/minus instead of side to side -->
                                        <thead class="thead-light">
                                        <tr>

                                            <th class="all">Name</th>
                                            <th>Total Sent</th>
                                            <th>Quantity</th>
                                            <th>Favored Method</th>
                                            <th>Added Date</th>
                                            <th>Kinks</th>
                                            <th>Status</th>
                                            <th style="width: 85px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        foreach ($subs as $l) {
                                            foreach($l as $id => $sub) {
                                                $amount = 0.00;
                                                $favored = "Unknown";
                                                $total = 0;
                                                if($sub['type'] == 1) {
                                                    $favored = "Bitcoin";
                                                    // That's me, check the transactions table
                                                    $transactionCursor = $transactionsCollection->find(['type' => 'send']);
                                                    foreach ($transactionCursor as $transaction) {
                                                        $total++;
                                                        $amount += floatval($transaction['cash_amount']);
                                                    }
                                                }else {
                                                    // Someone else, check the payments table
                                                    $methods = [];
                                                    $paymentCursor = $paymentsCollection->find(['person' => new MongoDB\BSON\ObjectId($id)]);
                                                    foreach ($paymentCursor as $payment) {
                                                        $total++;
                                                        $amount += floatval(str_replace(',','.',$payment['amount']));
                                                        array_push($methods, $payment['method']);
                                                    }
                                                    if($total > 0) {
                                                        $values = array_count_values($methods);
                                                        arsort($values);
                                                        $favored = array_slice(array_keys($values), 0, 1, false)[0];
                                                    }
                                                }
                                                ?>

                                                <tr>

                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-16 sender-name">
                                                            <span class="sender-name" data-name="<?php echo $id; ?>"><?php echo $sub['name']; ?></span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        echo $amount . " &euro;";
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php echo $total; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $favored; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $sub['added']; ?>
                                                    </td>
                                                    <td>
                                                        <span data-kinks="<?php echo $id; ?>"><?php echo $sub['kinks']; ?></span>
                                                    </td>
                                                    <td>
                                                        <?php

                                                        if($sub['active']) {
                                                            echo '<span class="badge badge-success">Active</span>';
                                                        }else {
                                                            echo '<span class="badge badge-danger">Inactive</span>';
                                                        }
                                                        ?>

                                                    </td>

                                                    <td class="table-action">
                                                        <a class="action-icon open-edit-sub" data-toggle="modal" data-target="#edit-sub-modal" data-id="<?php echo $id; ?>" data-name="<?php echo $sub['name']; ?>" data-status="<?php echo $sub['active']; ?>"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                        <a data-toggle="modal" data-target="#delete-alert-modal" class="open-delete-anchor action-icon" data-id="<?php echo $id; ?>" data-name="<?php echo $sub['name']; ?>" data-kinks="<?php echo $sub['kinks']; ?>"  > <i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
</div>