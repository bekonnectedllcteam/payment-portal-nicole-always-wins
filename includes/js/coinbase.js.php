<?php
if(!defined('access')){
    ob_end_clean();
    header("HTTP/1.1 403 Forbidden" );
    die('403 Forbidden');
}
ob_clean();
header('Content-type: text/javascript');
?>
$(function () {
    const apikey = "<?php echo $_SERVER['SAPIKEY']; ?>";
    const secret = "<?php echo $_SERVER['SAPISEC']; ?>";
    var sending = false;
    var socket = io({
        path: '/sio/socket.io'
    });
    socket.on("connect", function () {
        console.log("Connected to server!");
        $('#server-status').text("Connected to server, registering keys...");
        socket.emit('register_ks', {
            key: apikey
        });
    }).on('connect_error', function(error) {
        console.log("Error connecting?");
        console.log(error);
    }).on('error', function(error) {
        console.log("General error?");
        console.log(error);
    }).on('disconnect', function(reason) {
        $('#server-status').text("Disconnected from the server. Attempting to reconnect...");
        console.log("Disconnected");
        console.log(reason);
    }).on('register_ks', function (data) {
        if(typeof data.found != "undefined" && data.found === true) {
            console.log("Successful registration with server!");
            $('#server-status').text("Connected and registered with server!");
            var ts = Math.floor(Date.now()/1000);
            var ver = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(""+ts,secret));
            socket.emit("get_balance", {
                ut: ts,
                v: ver
            });
        }else {
            console.log("There was an error registering with the server!");
        }
    }).on("got_balance", function (data) {
        if(typeof data.status != "undefined" && data.status === 200) {
            var balance = data.data.balance;
            $("#euro-balance").text(balance+" €");
        }else {
            $("#server-status").html("There was an error getting account balance!");
        }
    }).on('send_return', function (data) {
        sending = false;
        if(typeof data.status != "undefined" && data.status === 200) {
            var ts = Math.floor(Date.now()/1000);
            var ver = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(""+ts,secret));
            socket.emit("get_balance", {
                ut: ts,
                v: ver
            });
            var btcAmount = data.data.btcAmount;
            var cashAmount = data.data.cashAmount;
            $("#server-status").text("Successfully sent " + btcAmount + " ($"+cashAmount+") to Nicole!");
        }else if(typeof data.status != "undefined" && data.status === 500 && typeof data.data != "undefined" && typeof data.data.message != "undefined") {
            $("#server-status").html("There was an error sending: " + data.data.message);
        }else {
            $("#server-status").html("There was an error sending!");
        }
    });
    $("#refresh-balance").on("click", function () {
        $("#euro-balance").text("-----");
        console.log("Refreshing balance!");
        var ts = Math.floor(Date.now()/1000);
        var ver = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(""+ts,secret));
        socket.emit("get_balance", {
            ut: ts,
            v: ver
        });
    });
    $('#send-button').on("click", function () {
        if(!sending) {
            var amount = $("#amount-to-send").val();
            if(amount.length > 0) {
                sending = true;
                var ts = Math.floor(Date.now() / 1000);
                var ver = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256("" + ts + "" + amount, secret));
                socket.emit("send_to_nicole", {
                    ut: ts,
                    amount: amount,
                    v: ver
                });
                $("#amount-to-send").val("");
                $("#server-status").text("Attempting to send " + amount + " € to Nicole!");
            }else {
                $("#server-status").text("You must enter an amount to send!");
            }
        }else {
            $("#server-status").text("Send request already in progress!");
        }

    });
});
<?php
    exit();
?>
