<?php
define("access", "index");
/** @noinspection PhpIncludeInspection */
require "/var/www/vendor/autoload.php";

include_once "includes/conf/loader.conf.php";

$pages = explode('/', $_SERVER['REQUEST_URI']);
array_shift($pages);
$page = $pages[0];

$ip = $_SERVER['REMOTE_ADDR'];

ob_start(); // Initiate the output buffer

$needed = [];
$add_to_end = [];
?>
<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Payment Portal - Nicole Always Wins</title>
        <meta content="Nicole's Pet" name="author" />
        <?php
        /** @noinspection PhpUndefinedVariableInspection */
        if($uid != null) {
            if($page == "subs" || $page == "payments") {
                ?>
                <!-- third party css -->
                <link href="/assets/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
                <link href="/assets/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
                <?php
            }
        }
        ?>
        <link href="/assets/css/icons.min.css?v=1" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="/assets/css/app-modern.min.css?v=1" type="text/css">
        <link rel="stylesheet" type="text/css" href="/assets/css/main.css?v=20200820_1447E" />
        <?php
        if($uid != null) {
            if($page == "payments"){
                ?>
                <link href="/assets/css/main.payments.css?v=20200821_1701E" rel="stylesheet" type="text/css" />
                <?php
            }elseif($page == "subs") {
                ?>
                <link href="/assets/css/main.subs.css?v=20200821_1701E" rel="stylesheet" type="text/css" />
                <?php
            }else {
                ?>
                <link href="/assets/css/main.dashboard.css?v=20200821_1701E" rel="stylesheet" type="text/css" />
                <?php
            }
        }
        ?>
    </head>
    <?php
    if($uid != null) {
        switch ($page) {
            case "":
            case "index":
            case "index.php":
            case "index.html":
            case "index.htm":
            case "index.asp":
                include "includes/pages/index.php";
                break;
            case "logout":
                include "includes/pages/logout.php";
                break;
            case "subs":
                include "includes/pages/subs.php";
                break;
            case "payments":
                include "includes/pages/payments.php";
                break;
            case "coinbase":
                include "includes/pages/coinbase.php";
                break;
            case "assets":
                if($pages[1] == "js" && $pages[2] == "coinbase.js") {
                    include "includes/js/coinbase.js.php";
                }
                break;
            default:
                include "includes/pages/errors/404.php";
                break;
        }
    }else {
        switch ($page) {
            case "forgot":
                include "includes/pages/forgot.php";
                break;
            case "login":
            default:
                include "includes/pages/login.php";
                break;
        }
    }
    include "includes/pages/footer.php";
    ?>
</html>